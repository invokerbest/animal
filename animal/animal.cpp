﻿#include <iostream>
#include <string>
using namespace std;
class Animal
{
public:
	virtual void Voice()
	{}
};
class Dog : public Animal
{
public:
	void Voice() override
	{
		string a = "woof";
		cout << a << '\n';
		
	}
};
class Cat : public Animal
{
public:
	void Voice() override
	{
		string b = "Meow";
		cout << b << '\n';
		
	}
};
class Cow : public Animal
{
public:
	void Voice() override
	{
		string c = "Moo";
		cout << c << '\n';

	}
};

int main()
	{
	Dog a;
	Cat b;
	Cow c;

	Animal* bar[3] = { &a,&b,&c };
	for (int i = 0; i < 3; i++)
	bar[i]->Voice();
	}
	
